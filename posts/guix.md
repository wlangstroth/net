title: Guix
date: 2020-05-26 08:00
tags: guix,devops,guile
summary: The guix package manager
---
I love the idea of Guix. Lots of buzzwords. "Functional", "clean", "transational". It all sounds great. I still haven't figured the damn thing out, but here are the notes, because the documentation is confusing. 

To install Guix as a package manager, switch to the `root` user and:

```
# cd /tmp
# wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
# chmod +x guix-install.sh
# ./guix-install.sh
```

There you are half done. 

Unfortunately, the script doesn't quite do it on systems using systemd, like debian (or raspberry pi os).
https://issues.guix.info/41356
