title: Haunt
date: 2020-05-26 12:30
tags: guile,website
summary: Wherein I post notes about the haunt static site compiler
---
Haunt doesn't come with a whole lot of documentation. The example is almost entirely unhelpful, but thankfully the author of Haunt has his own site's code in the repository, so you can look at that to see how one might actually make a website.

The website doesn't quite get it right, because there's no `<html>` tag, so you'll have to look out for that

## How to do it

Naturally, you are using [guix](https://guix.gnu.org/). What's that? You're not? Goodness, however do you manage? We have [help for that](/posts/guix.html).

I'm going to outline how to set up a bootstrap 4.5 page.

...

Here's why I like parenthetical languages for HTML:

```
<html>Some stuff </html>
```

```
(html "Some stuff")
```

