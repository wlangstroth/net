title: Done With macOS
date: 2020-06-28 22:00
tags: computing
summary: In which is described my loathing of the macOS decline
---

Remember OSX? When Steve Jobs came back to Apple with a vengeance, he brought his NeXT/Unix/BSD stuff with him. Which is good, because NeXT was actually a decent product, it just didn't have enough of a market. NeXT was smooshed into OSX, and if I recall correctly, it worked on IBM's POWER chips, which were amazing. Of course, how are you supposed to make any money on amazing things? That never happens. So Apple, being a publicly traded corporation, was forced - by the nature of publicly traded corporations - to cheapen the whole thing and use Intel chips. The horror! There was a fair amount of gnashing of teeth and pulling of hair among the Apple loyalists, but I think by that time, everyone knew that Intel was an unstoppable monster.

At that point, OSX was still Unix-y, and it really felt more like you were using a BSD machine when you brought up the Terminal program. For the console users like me, it was great. You had something that would play videos for the kids, but still felt like a real BSD machine. You could download emacs and you could compile it! In theory! Well, sometimes you could get it to work.

Anyway!

That was probably 2005 or thereabouts, when I had been using Linux for about four years. Because Windows was just about the worst, and I had been taken in by Richard Stallman's poetic idea of source code as freedom. I didn't know at the time that he was a sexist shit, but that's another blog post.
