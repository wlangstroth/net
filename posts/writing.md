title: Writing
date: 2020-05-25 18:00
tags: writing
summary: Some writing about writing
---
Here I record some thoughts.

Not important thoughts. Not deep thoughts. Just thoughts.

They are recorded here in case they eventually make sense. Or at least more sense than Tristram Shandy, which is crazy.
