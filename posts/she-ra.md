title: She-Ra, of All Things
date: 2020-05-29 08:30
tags: nostalgia,toys,cartoons
summary: Cartoons
---
I kind of remember the original She-Ra television show and toy line. I had He-Man toys when they came out in 1983, because like many kids, I was just the right age to think they were the greatest thing ever. The toys came out before the Saturday morning cartoons, I think, but

...

For the sake of my one daughter, who is six, and into all sorts of things, I will occasionally watch a show to see if it will turn her into a monster. So I watched "She-Ra and the Princesses of Power" expecting basically something about the same level as My Little Pony, but probably worse.

Boy was I in for a treat. Because I love satire, and this was some deep satire.

I don't know if it helps to remember any of the original shows. They were produced by Filmation, which has its own fascinating history. If you were a kid in the 80s, you know [all the shows they made](https://en.wikipedia.org/wiki/List_of_works_produced_by_Filmation) by style. A style they apparently learned from the the 1960s Spider Man and Rocket Robin Hood series.

When I looked up Filmation, I expected them to be responsible for both Spider Man and Rocket Robin Hood, so close were the styles.

...

This new She-Ra is impressive for a number of reasons. For one thing, it's super gay. There are gay dads, a gay princess couple, and some other gay that would be a spoiler, I think. That may seem to come out of nowhere, but Filmation was apparently filled with gay.
