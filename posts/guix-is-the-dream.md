title: Guix is The Dream
date: 2020-06-02 10:00
tags: guix,guile,devops,systems,linux,gnu
summary: With Guix, the dream is in our grasp
---

I'm a huge Free Software fanboy. I've been installing Linux variants on old hardware since Red Hat 6.0, which worked just fine on two 386 machines that I fished out of a corporate dumpster along with their monitors. And all that the monitors needed was a new fuse. It was awesome.

These days, the hardware is cheaper and better, and the software has quietly gotten about a million times better. 


GNU Mes
https://www.gnu.org/software/mes/

Holy mother of GNU. Shit just got real.
