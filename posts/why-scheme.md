title: Why I Like Scheme
date: 2020-06-25 12:00
tags: guile,scheme
summary: Scheme is amazing and eternal
---

In the world of software, things are always changing. New languages, new libraries, new package managers, all sorts of new things. Bugs are getting found and fixed all the time, and it's 

If you're using a language like JavaScript in your daily life, the pace of change is frankly annoying. I haven't had to do any JavaScript in years, but the last time I did, angular.js and CoffeeScript had established themselves in enough codebases to ensure their immortality, and I think grunt or something was the package manager. Thankfully, I don't need to keep up with that anymore.

Now I'm just a hobby programmer, and I'm using Guile for everything, because there's nobody to tell me I can't. Guile is a scheme dialect, and scheme has been around forever (in computing terms). So much so that 

The string library (srfi-13) hasn't been updated for 20 years. 20 years! If that were a JavaScript library, that would be terrifying. You wouldn't go near it! There must be so many bugs! It would be unusable, outdated garbage. But in scheme, that's the string library, because it works just fine.

That's the best thing about scheme, as far as I'm concerned. It's not going to be the new hotness anywhere, and people aren't going to write tons of blog posts about it or whatever, but the libraries are stable and reliable.

Now, to be fair, scheme needs a string library, and JavaScript's is built in. If we were making a comparison strictly on this one library, JavaScript's string "library" is also technically stable.
