title: The News
date: 2020-06-26 15:13
tags: news
summary: We live in the craziest times
---

This will probably make no sense, but I'll write it down anyway.

I can't watch the news anymore.

Here's the problem: there's nothing really that new. Okay, the Supreme Court decision to follow Canada in making discrimination against LGBTQ people illegal was huge. That's news.

The rest is sort of garbage, and not because there's nothing to tell, but because to understand it would take a degree in history. The middle east is the way it is because of history. Lots of history. The reason a lot of the middle east sees us (the west) as the enemy is because the crusades are the kind of thing that a group of people aren't going to let go.

If Americans had something like that, they would *never* let it go. Like, never. Pearl Harbor or 9/11 pales in comparison to hundreds of years knocking down doors and messing with people. First, it was for the "Holy Land", by which Europeans of course meant land to be conquered. Then it was all about oil, which might as well be holy in the current era.
