title: Not Ready for Prime Time
date: 2020-07-04 23:00
tags: free-software,gnu,guix
summary: OMG people are jerks
---

People are always trying to ruin other people's fun. I don't know why. Maybe it makes them feel good about themselves in some way. But inherent in human nature is ruining other people's fun, and software is a perfect example of how it plays out.

Take, for example, The GNU Project.

The GNU Project is a volunteer effort to make a UNIX-like system (and utilities) that is built entirely in the open. This is known as "free software", but what that means isn't that it's free of charge, but that the source code is guaranteed never to be hidden. That's a bit different than "open source" which means you can view the source, but there's no guarantee that it won't be hidden from view tomorrow.

It's "free" as in "freedom", but it's difficult to explain why that's important to people without some history.


The GNU Project is all about not being ready for prime time. The GUIX System,
for which I have a great deal of praise, is clearly not ready for prime time.
It's great as a practical research project and provides us with a light in the
increasing darkness (and by darkness, I mean legislation to add back doors to software).

...

If people are working on a cure for cancer, they don't constantly piss and moan about how they haven't cured cancer yet. The GNU System isn't ready yet for some people.

That's fine. Don't use it. But when legislation is being consistently pushed in the United States
to compel companies to add backdoors to software, then you know it's going to happen eventually. If your source code is legally required to be out in the open, then it's fine. We know exactly where those back doors are, and coders can close them.

Naturally, if it becomes illegal to close back doors to software, then we're going to have a bad time. 
