title: Firefox, Chromium and Hardware Acceleration
date: 2020-07-03 09:00
tags: corporations,linux
summary: Chromium doesn't do hardware acceleration, Firefox does if you force it
---
As soon as I'd set up Chromium and Firefox on one of my Linux machines (read: old computer that can't run Windows or macOS anymore becuse the poor things can't take the punishment) I noticed that both Chromium and Firefox exhibited those jittery lines when playing video characteristic of software video acceleration. I thought that was a bit strange, because Android and Chromebook devices are essentially running Linux, and there's no way they'd ship without hardware acceleration.

Chromium doesn't let you turn on hardware acceleration. It's dressed up as a licensing problem for things like H264, but there are lots of knobs and switches you need to mess with to make that work. Or so it seems. After reading about it a bit, I couldn't be bothered, but I'm sure someone else can solve that one.

In Firefox, you can enable hardware acceleration by entering `about:config` in the search bar. Then search for and select `layers.acceleration.force-enabled`. That's it. Now you have smoother scrolling and videos look great. Why would that be such a hidden feature?
