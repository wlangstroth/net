title: Politics by Other Means
date: 2020-05-30 08:00
tags: war,politics
Summary: On War
---
It was Carl von Clauswitz who called war "politics by other means". 

Weapons, in order of effectiveness:

1) Nuclear bombardment. This is the ultimate in "effective" weaponry. It kills people, it destroys everything, and irradiates 
Think of how scary the Japanese were to Americans that they called forth the power of Pandora's Box to destroy them. The greatest weapon ever unleashed upon The Earth, and that's how scary the Japanese were as an adversary. And that's after firebombing them into ashes. They wouldn't give up, they were proud, they were way better at war, and they were invincible. Unless you firebomb them for months on end and then finish it off with a nuclear strike. That's the only way to defeat them, though. 
