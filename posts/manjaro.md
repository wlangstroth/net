title: Manjaro
date: 2020-06-28 22:30
tags: linux
summary: Manjaro Linux is great
---

Call me a weirdo, but I love setting up Linux systems. I've always loved doing it. I love watching the packages load, reading the descriptions of the packages I don't recognize, looking up weird bioinformatics software I'll never use. Somehow, it never gets old. It's amazing all the work that goes into creating really good software, and how much of it is done by volunteers. So I've installed all of the Linux distributions. Okay, maybe not all of them, but most. They're mostly defined by their package managers, and I've used every package manager. Even "brew", for macOS, which is the most brute force effort of all of them (probably because it operates in hostile territory).

Today, I tried installing Manjaro with Manjaro Architect. I've installed Arch before, many times on many different types of system. Tiny netbooks, old server boxes, dying laptops. and it's great as long as you have a collection of notes on installing Arch. I don't know what you do if you haven't installed it a bunch of times. Even using Arch's version of Architect is a bit of an intimidating experience. You need to know a *lot* about what kind of system you're installing, and what kind of hardware you're matching to what drivers.

This wasn't the regular ncurses installer, though. They made it look good. Grey and cyan with all kinds of slick looking borders. People new to the experience will wonder why I'm making a big deal of a colour scheme, but when you've done the grey-on-blue a few times, it's nice to have someone fancy it up a little. And it was easy! Oh man it was easy.

And. AND! I got to try out the i3 window manager, which I've been wanting to try for a while. Now I'm sad I didn't before. It's my new favourite. Maybe it's the Manjaro configuration of i3, but it gets out of your way, there's [a cheat sheet](https://i3wm.org/docs/refcard.html) that becomes muscle memory very quickly, and you can use [dmenu](), [bmenu]() 

