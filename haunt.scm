(use-modules (haunt site)
             (haunt reader)
             (haunt asset)
             (haunt page)
             (haunt post)
             (haunt html)
             (haunt utils)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader commonmark)
             (srfi srfi-19) ; date
             (ice-9 rdelim)
             (ice-9 match)
             (web uri))

(define javascript-includes
  `((script (@ (src "https://code.jquery.com/jquery-3.5.1.slim.min.js")
               (integrity "sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj")
               (crossorigin "anonymous")))
    (script (@ (src "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js")
               (integrity "sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo")
               (crossorigin "anonymous")))
    (script (@ (src "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js")
               (integrity "sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI")
               (crossorigin "anonymous")))))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "/css/" name ".css")))))

(define (net-layout site title body)
  `((doctype "html")
    (html (@ (lang "en")
             (class "h-100"))
          (head
           (meta (@ (charset "utf-8")))
           (meta (@ (name "viewport")
                    (content "width=device-width, initial-scale=1, shrink-to-fit=no")))

           (link (@ (rel "stylesheet")
                    (href "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css")
                    (integrity "sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk")
                    (crossorigin "anonymous")))

           ,(stylesheet "style")
           (title ,(string-append title " — " (site-title site))))
          (body (@ (class "d-flex flex-column h-100"))
                (main (@ (role "main")
                         (class "flex-shrink-0"))

                      (div (@ (class "container"))
                           (a (@ (href "/"))
                              (h1 (@ (class "mt-5")) ,(site-title site)))
                           (p (@ (class "lead")) "Behold The Thinkerating")
                           ,body
                           (footer (@ (class "text-center"))
                                   (p (small "Created with too many parentheses")))))
                (script (@ (src "https://code.jquery.com/jquery-3.5.1.slim.min.js")
                           (integrity "sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj")
                           (crossorigin "anonymous")))
                (script (@ (src "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js")
                           (integrity "sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo")
                           (crossorigin "anonymous")))
                (script (@ (src "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js")
                           (integrity "sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI")
                           (crossorigin "anonymous")))))))

(define (post-template post)

  `((h2 ,(post-ref post 'title))
    (h3 "by " ,(post-ref post 'author)
        " — " ,(date->string (post-date post) "~Y-~m-~d"))
    (div ,(post-sxml post))))

(define (collection-template site title posts prefix)
    (define (post-uri post)
      (string-append "/" (site-post-slug site post) ".html"))
    `((ul
       ,@(map (lambda (post)
                `(p
                  "["
                  ,(date->string (post-date post) "~Y-~m-~d")
                  "] "
                  (a (@ (href ,(post-uri post)))
                     ,(post-ref post 'title))))
              (posts/reverse-chronological posts)))))

(define net-theme
  (theme #:name "net"
         #:layout net-layout
         #:post-template post-template
         #:collection-template collection-template))

(define %collections
  `(("Home" "index.html" ,posts/reverse-chronological)))

(site #:title "(expunge)"
      #:domain "langstroth.net"
      #:default-metadata
      '((author . "Will")
        (email  . "will@langstroth.com"))
      #:readers (list commonmark-reader)
      #:builders (list (blog #:theme net-theme #:collections %collections)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (static-directory "img")
                       (static-directory "css")))
